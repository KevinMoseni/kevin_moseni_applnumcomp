function varargout = CompAssn6(varargin)
% COMPASSN6 MATLAB code for CompAssn6.fig
%      COMPASSN6, by itself, creates a new COMPASSN6 or raises the existing
%      singleton*.
%
%      H = COMPASSN6 returns the handle to a new COMPASSN6 or the handle to
%      the existing singleton*.
%
%      COMPASSN6('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMPASSN6.M with the given input arguments.
%
%      COMPASSN6('Property','Value',...) creates a new COMPASSN6 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CompAssn6_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CompAssn6_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CompAssn6

% Last Modified by GUIDE v2.5 08-Dec-2017 12:40:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CompAssn6_OpeningFcn, ...
                   'gui_OutputFcn',  @CompAssn6_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});

% End initialization code - DO NOT EDIT
end

% --- Executes just before CompAssn6 is made visible.
function CompAssn6_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CompAssn6 (see VARARGIN)

% Choose default command line output for CompAssn6
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

clear
save('UserInputsDS.mat')
save('UserInputsDO.mat')
save('UserInputsDE.mat')

% UIWAIT makes CompAssn6 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = CompAssn6_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in ButtonDS.
function ButtonDS_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

openfig('DefineSystemofEquations');
uiwait
load('UserInputsDS.mat')

NumODEs = str2double(NumODEs);
handles.allInitialValueODE = [];
    for i = 1:NumODEs
        
        % open the GUI for user inputs for ODE
        DefineODE
        uiwait
        
        % load the user inputs from other GUI
        load('UserInputsDO.mat')
        uiresume
        
        % pulling the values from the ODE GGUI
        handles.alldTop{i} = dTop;
        handles.alldBottom{i} = dBottom;
        handles.allInitialValueODE(i) = InitialValueODE;
        handles.allODErhs{i} = ODErhs;
        
        handles.list = get(handles.ODEslist,'String');
        handles.list{end+1} = ['d',dTop,'/','d',dBottom,'=',ODErhs];
        set(handles.ODEslist,'String',handles.list)
        
        inlist = get(handles.Initiallist,'String');
        inlist{end+1} = [dTop,'(0)=',num2str(InitialValueODE)];
        
        set(handles.Initiallist,'String',inlist)
        
        % Update handles structure
        guidata(hObject, handles);
    
    end

NumAlgEs = str2double(NumAlgEs);

    for i = 1:NumAlgEs 
        % open GUI for defining the Expl. Eqs
        DefineExplicitEquation
        uiwait
        
        % pulling values from other GUi
        load('UserInputsDE.mat')
        uiresume
        
        handles.allExpRHS{i} = ExpRHS;
        handles.allExpLHS{i} = ExpLHS;
        handles.allExplicit{i} = strcat(ExpLHS,'=',ExpRHS);
        
        % to display user inputs on main GUI
        Explist = get(handles.Explicitlist,'String');
        Explist{end+1} = [ExpLHS,'=',ExpRHS];
        set(handles.Explicitlist,'String',Explist)
        
        % Update handles structure
        guidata(hObject, handles);
        
    end
    
    % ask user for the limits of integration and outputs them to the main
    % GUI list box
    prompt = {'Lower limit:','Upper limit:'};
    dlg_title = 'Limits of Integration';
    num_lines = 1;
    defaultans = {'0','5'};
   
    limits = inputdlg(prompt,dlg_title,num_lines,defaultans);
    
    set(handles.list_limits,'String',limits)
    handles.t = handles.list_limits;
    
    % Update handles structure
    guidata(hObject, handles);
    
function dydt = ODE(t, y, alldBottom, alldTop, allODErhs, allExplicit)
% Input: t and y are the independent and dependent variable values
% denominators, numerators, RHSs, and explicitEqns are cell
% arrays with the first three terms defining the ODEs of ... the form
% d(numerators) / d(denominators) = RHSs and the fourth term
% defining the associated explicit equations
% Output: the derivative vector dy/dt for y(1):y(numODEs)where
% numODEs = length(numerators) = length(RHSs) = length(denominators)

% independent variable converted to math
str0=cell2mat(alldBottom(1));
eval(strcat(str0,'=t;'));

% dependent variables converted to math
for i = 1:length(alldTop)
str1 = (cell2mat(alldTop(i)));
eval(strcat(str1,'=y(i);'));
end

% explicit equations provided in MATLAB acceptable order can be semicolon 
% separated list
for i = 1:length(allExplicit)
str2 = cell2mat(allExplicit(i));
eval(strcat(str2,';'));
end

% Right-hand sides of ODE definitions converted to math
for i = 1:length(allODErhs)
str3 = cell2mat(allODErhs(i));
dydt(i) = eval(str3);
end

dydt = dydt';    

% --- Executes on button press in ButtonCR.
function ButtonCR_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% changing the t from string to double array to feed into ode45
tspanstr = get(handles.list_limits,'String');
tspan = [str2double(tspanstr(1)) str2double(tspanstr(2))];

% uses ode45 with all the user specified variables 
[TOUT,YOUT] = ode45(@(t,y)ODE(t, y, handles.alldBottom,handles.alldTop, handles.allODErhs, handles.allExplicit), tspan, handles.allInitialValueODE);
msgbox('Calculation complete.')

handles.TOUT = TOUT;
handles.YOUT = YOUT;

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in ButtonPR.
function ButtonPR_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonPR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

t = handles.TOUT;
y = handles.YOUT;
    
    % only the y(i) will be used, to minimize clutter in list dialogue box
    str = handles.alldTop;
    
    [s] = listdlg('PromptString','Select Independent Variable:','SelectionMode','multiple','ListString',str);

    cla(handles.axes1)
    hold on
    
    % handles.A will be used by xlswrite in the export button callback
    handles.A = [t, y];
    
    % matches the indices for [s] and the y(i)'s that the user wants to
    % plot
    for i = 1:length(s)
        desiredSoln = y(:,s(i));            
        % Create  plot in proper axes
        plot(t,desiredSoln,'DisplayName',str{s(i)});
    end
hold off    
legend('Location','Best')  
grid on
set(handles.axes1,'XMinorTick','on');

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in ButtonSP.
function ButtonSP_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ask user for the desired directory
handles.foldername = uigetdir();

% ask user for the file name
handles.fileName1 = inputdlg('Enter a file name','File name',1);

    % check to see if the name exists, if not it will print to a .png with a
    % user specified name to a user specified folder.
    if exist('fileName', 'file') == 2
    h = msgbox('This file name already exists. Enter a different one!','Plot NOT saved.');
    else
    ax = handles.axes1;
    figure_handle = isolate_axes(ax);
    export_fig(char(handles.foldername),sprintf('SolutionPlot_%s',char(handles.fileName1)), '-png');
    h = msgbox('The plot was successfully saved as a .png','Plot Saved');
    end


% --- Executes on button press in ButtonE.
function ButtonE_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ask user for name for the exported raw data to excel
prompt = {'File name for raw data'};
    dlg_title = 'Export raw data';
    num_lines = 1;
    defaultans = {'rawdata.xlsx'};
   
    filename = inputdlg(prompt,dlg_title,num_lines,defaultans);
% uses the user specified name as the name of the raw excel file with data
    A = handles.A;
    xlswrite(char(filename),A)

% --- Executes on selection change in ODEslist.
function ODEslist_Callback(hObject, eventdata, handles)
% hObject    handle to ODEslist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ODEslist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ODEslist

% --- Executes during object creation, after setting all properties.
function ODEslist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODEslist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in Initiallist.
function Initiallist_Callback(hObject, eventdata, handles)
% hObject    handle to Initiallist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Initiallist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Initiallist


% --- Executes during object creation, after setting all properties.
function Initiallist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Initiallist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Explicitlist.
function Explicitlist_Callback(hObject, eventdata, handles)
% hObject    handle to Explicitlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Explicitlist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Explicitlist


% --- Executes during object creation, after setting all properties.
function Explicitlist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Explicitlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in list_limits.
function list_limits_Callback(hObject, eventdata, handles)
% hObject    handle to list_limits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns list_limits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_limits


% --- Executes during object creation, after setting all properties.
function list_limits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_limits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
