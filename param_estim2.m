%Author: Kevin Moseni
%Date: 10/9/2017

%%Lecture 12: Parameter Estimation Example 2
function param_estim2

xdata = [0.5 1 5 20];   %time
ydata = [99, 98, 50, 3; 2, 4, 35, 7];   %x1;x2


x0(1)= 100;
y0(2)= 1;

b1guess = 1;
b2guess = 2;

parameterguesses = [b1guess, b2guess];

    function output = ODEmodel(parameters, t)

            [tsoln,xsoln] = ode45(@(t,x1,x2)deriv(t,x1,x2,parameters),t, xdata);    
            output = xsoln';    %the array needed to be reoriented
    end

    function dxdt = deriv(t,x,parameters)           
        b1 = parameters(1);
        b2 = parameters(2);
        dxdt = b1-b2*x;
        
    end

[parametersoln, resnorm,residual,exitflag,output] = lsqcurvefit(@ODEmodel, parameterguesses, tdata, xdata)
end