% y = [1, 4, 9, 16];
% x = 2*[1, 2, 3, 4];
% 
% plot(y)     %only plots the y-values with index values on x-axis
% hold on
% plot(x,y)   %plots x and y values, in this case x-values are the same as the indeces 
% hold off        
% legend('y','x,y')   %string is denoted using single quotes 
% 
% xlabel('x')
% ylabel('y = x^2')   %can handle some LaTeX formatting (this will superscript 2)
% 
% %% set x and y-axis limits
% axis([0,10,0,20])   %
% 
% xmin = 0;   %manually set the window of view
% xmax = 5;
% ymin = 2;
% ymax = 20;
% axis([xmin, xmax, ymin, ymax])
% 
% %% evenly sampled times at 0.2 s intervals
% t = linspace(0,5,26); %26 evenly spaced points in the interval of 0
% t = 0:0.2:5 %starttime:increment:finaltime
% 
% %% symbols, colors, & line styles
% %plot(x,y, 'ro--')   %red, circle markers, with large dashed line
% plot(x,y, 'ro-.')   %red, circle markers, with dash-dot line
%==========================================================================
%==========================================================================
%In class problem
y = [1, 4, 9, 16];
x = 2*[1, 2, 3, 4]; 

%% set x and y-axis limits
axis([0,10,0,20])   %sets the window if view xmin, xmax, ymin, ymax

xmin = 0;   %manually set the window of view
xmax = 5;
ymin = 2;
ymax = 20;
axis([xmin, xmax, ymin, ymax])

%% evenly sampled times at 0.2 s intervals
t = linspace(0,5,26); %26 evenly spaced points in the interval of 0
t = 0:0.2:5 %starttime:increment:finaltime

%% symbols, colors, & line styles
plot(x,y, 'ro--')   %red, circle markers, with large dashed line
plot(x,y, 'ro-.')   %red, circle markers, with dash-dot line
hold on

% plot(t, t, 'r--') %see below, can also list all in one plot() without
% hold on/off

% hold on
% plot(t, t.^2, 'bs')
% hold on 
% plot(t, t.^3, 'g^')
% hold off
% legend('x, y','t, t', 't, t^2', 't, t^3')

plot(x,y, 'ro-', t, t.^2, 'bs', t, t.^3, 'g^')
legend('t','t^2','t^3', 'location', 'best')

%data = csvread('inflammation-01.csv');
data = ones(20);

figure(2)   %this just opens a new figure windown and label it 2, otherwise it will keep adding to the same figure as previously.
subplot(1,3,1)
plot(mean(data,1)) %second value is the row
ylabel('average')

subplot(1,3,2)
plot(max(data,[],1))
ylabel('max')






