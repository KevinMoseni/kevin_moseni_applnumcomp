%% Author: Kevin Moseni
% Date: 10/24/2017
% CHE 4990: Applied Numerical Computing
% Computational Assignment #4

% Components: VGO, Gasoline, Gas, and Coke 
% FCC: 3 Lump Model
% this program evaluates 4 ODEs to solve for the changing weight fraction of 
% 4 components. This version combines gas and coke.

%% variables
% t  - time
% X  - conversion (defined as [1-y_1])
% y_1 - VGO
% y_2 - gasoline
% y_3 - the sum of gas and coke

function param_estim_3lump
%% data from Table 1: Data for FCC process.
xdata = [1/60, 1/30, 1/20, 1/10]; % time data

%% arranging ydata into a matrix
y1data = [0.5074, 0.3796, 0.2882, 0.1762];   % weight fraction VGO
y2data = [0.3767, 0.4385, 0.4865, 0.5416];   % weight fraction Gasoline
Gasdata = [0.0885, 0.136, 0.1681, 0.2108];      % weight fraction Gas
Cokedata = [0.0274, 0.0459, 0.0572, 0.0714];    % weight fraction Coke
y3data = Gasdata + Cokedata;
ydata = [y1data; y2data; y3data]';


%% guesses for parameters k1, k2, & k3
k(1) = 0.1;
k(2) = 0.2;
k(3) = 0.3;
params_guess = k;

%% initial conditions for ODEs  % initial weight fractions
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;

%% options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-14, 'TolFun', 1e-14,'StepTolerance',1e-14);

%% estimate parameters
[params,resnorm] = lsqcurvefit(@(params,xdata)ODEmodel1(params,xdata,y0),params_guess,xdata,ydata,[], [], OPTIONS);

%% plot data and the curve fit
figure(1)
hold on
axis([0,1,0,1])
y_calc = ODEmodel1(params,xdata,y0);
plot((1-y_calc(:,1)'),ydata(:,1),'rx')   %conversion is described as 1-y_1
plot((1-y_calc(:,1)'),ydata(:,2),'b+')   %so the plot will simply convert to
plot((1-y_calc(:,1)'),ydata(:,3),'g.')   %conversion on the x-axis.

xlabel('Conversion, wt frac.')
ylabel('Yield, wt frac.')
                                        %the ODE solver provides the soluti
y1_calc = y_calc(:,1);                  %ons in a 3 by 3 array that must th
y2_calc = y_calc(:,2);                  %en be scanned by the plotter, but
y3_calc = y_calc(:,3);                  %instead the array was unpacked.
plot((1-y_calc(:,1))',y1_calc,'r-')
plot((1-y_calc(:,1))',y2_calc,'b-')
plot((1-y_calc(:,1))',y3_calc,'g-')
hold off
export_fig(sprintf('CA4_3_lump_conversion'), '-png');
legend('VGO','Gasoline','Gas+Coke','Predicted VGO','Predicted Gasoline','Predicted Gas+Coke')

figure(2)
hold on
axis([0,1,0,1])
plot(xdata',ydata(:,1),'rx')      %conversion is described as 1-y_1
plot(xdata',ydata(:,2),'b+')  %so the plot will simply convert to
plot(xdata',ydata(:,3),'g.')  %conversion on the x-axis.

xlabel('t, hr')
ylabel('Yield, wt frac.')
y_calc = ODEmodel1(params,xdata,y0);    %the ODE solver provides the soluti
y1_calc = y_calc(:,1);                  %ons in a 3 by 3 array that must th
y2_calc = y_calc(:,2);                  %en be scanned by the plotter, but
y3_calc = y_calc(:,3);                  %instead the array was unpacked.
plot(xdata',y1_calc,'r-')
plot(xdata',y2_calc,'b-')
plot(xdata',y3_calc,'g-')
hold off
export_fig(sprintf('CA4_3_lump_time'), '-png');
legend('VGO','Gasoline','Gas+Coke','Predicted VGO','Predicted Gasoline','Predicted Gas+Coke')
end

%% define the ODEs
% Uses params = [k_1, k_2, k_3]

function dydt = ODE(t,y,params)
k_1 = 38.9;    % assigning names to the three different
k_2 = 1.85;    % parameters
k_3 = 13.2;

dydt(1) = -1*(k_1+k_3)*y(1)^2;  % $\frac{dy_1}{dt}=-(k_2+k_3)*{y_1}^2$
dydt(2) = k_1*y(1)^2-k_2*y(2);  % $\frac{dy_2}{dt}=k_1*{y_1}^2-k_2*y_2$
dydt(3) = k_3*y(1)^2+k_2*y(2);  % $\frac{dy_3}{dt}=k_3*{y_1}^2+k_2*y_2$
dydt = dydt';   % reorient the matrix
end

%% solving the ODEs
% Uses current params values and xdata as the final point in the tspan for the ODE solver

function y_output = ODEmodel1(params,xdata,y0)
for i = 1:length(xdata)
    tspan = [0:0.001:xdata(i)];
    [~,y_calc] = ode23s(@(t,x) ODE(t,x,params),tspan,y0);
    y_output(i,:)=y_calc(end,:);
end
end