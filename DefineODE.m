function varargout = DefineODE(varargin)
% DEFINEODE MATLAB code for DefineODE.fig
%      DEFINEODE, by itself, creates a new DEFINEODE or raises the existing
%      singleton*.
%
%      H = DEFINEODE returns the handle to a new DEFINEODE or the handle to
%      the existing singleton*.
%
%      DEFINEODE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEODE.M with the given input arguments.
%
%      DEFINEODE('Property','Value',...) creates a new DEFINEODE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DefineODE_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DefineODE_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DefineODE

% Last Modified by GUIDE v2.5 09-Dec-2017 19:57:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DefineODE_OpeningFcn, ...
                   'gui_OutputFcn',  @DefineODE_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DefineODE is made visible.
function DefineODE_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DefineODE (see VARARGIN)

% Choose default command line output for DefineODE
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DefineODE wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = DefineODE_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function ODELHStop_Callback(hObject, eventdata, handles)
% hObject    handle to ODELHStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ODELHStop as text
%        str2double(get(hObject,'String')) returns contents of ODELHStop as a double

% extract user input data
handles.ODELHStop = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ODELHStop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODELHStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ODERHS_Callback(hObject, eventdata, handles)
% hObject    handle to ODERHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ODERHS as text
%        str2double(get(hObject,'String')) returns contents of ODERHS as a double

% extract user input data
handles.ODERHS = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ODERHS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODERHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ODEinitialvalue_Callback(hObject, eventdata, handles)
% hObject    handle to ODEinitialvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ODEinitialvalue as text
%        str2double(get(hObject,'String')) returns contents of ODEinitialvalue as a double

% extract user input data
handles.initialvalue = str2double(get(hObject,'String'));
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ODEinitialvalue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODEinitialvalue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ODELHSbottom_Callback(hObject, eventdata, handles)
% hObject    handle to ODELHSbottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ODELHSbottom as text
%        str2double(get(hObject,'String')) returns contents of ODELHSbottom as a double

% extract user input data
handles.ODELHSbottom = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ODELHSbottom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODELHSbottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ODESubmitButton.
function ODESubmitButton_Callback(hObject, eventdata, handles)
% hObject    handle to ODESubmitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% use the follow .mat to transfer data from this GUI to main
load('UserInputsDO.mat')

% collects user inputs
dTop = handles.ODELHStop;
dBottom = handles.ODELHSbottom;
ODErhs = handles.ODERHS;
InitialValueODE = handles.initialvalue;

% save the desired variables
save('UserInputsDO.mat','dTop','dBottom','InitialValueODE','ODErhs')

close
