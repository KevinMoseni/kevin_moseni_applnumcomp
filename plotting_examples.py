# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 08:53:14 2017

@author: kevin
"""

import numpy as np
import matplotlib.pyplot as plt

x = np.array([1,2,3,4])
x=x*2

y = np.array([1,4,9,16])
plt.plot(y)
plt.ylabel('some numbers')
plt.show()  #kind of like hold off

plt.plot(x,y) #python will hold on by default
plt.show()

plt.plot(x,y,'ro--')
xmin = 0.
xmax = 5.
ymin = 2.
ymax = 20.
plt.axis([xmin,xmax,ymin,ymax])

t = np.arange(0.,5.1,0.2)

plt.plot(t,t,'r--',t,t**2,'bs',t,t**3,'g^')

data = np.loadtext('inflammation-01.csv'),delimiter =

fig = plt.figure(figsize = (10.0m 3.0))
axes1 = fig.add_subplot(1,3,1)
axes2 = fig.add_subplot(1,3,2)
axes2 = fig.add_subplot(1,3,3)

axes1.set_ylabel('average')
axes1.plot(np.mean(data,axis=0))

axes2.set_ylabel('max')
axes2.plot(np.max(data,axis=0))

axes3.set_ylabel('min')
axes3.plot(np.min(data,axis=0))

plt.show()
