"""
 Author: Kevin Moseni
 Date: 10/24/2017
 CHE 4990: Applied Numerical Computing
 Computational Assignment #4

 Components: VGO, Gasoline, Gas, and Coke 
 FCC: 3 Lump Model
 this program evaluates 4 ODEs to solve for the changing weight fraction of 
 4 components. This version combines gas and coke.

 variables
 t  - time
 X  - conversion (defined as [1-y_1])
 y_1 - VGO
 y_2 - gasoline
 y_3 - the sum of gas and coke
"""
import numpy as np #needed for all numerical methods
from scipy.integrate import odeint #this is essentially ode45
import matplotlib.pyplot as plt #for plotting
from scipy.optimize import curve_fit

#def param_estim_3lump():
 
xdata = np.array([1/60,1/30,1/20,1/10]) # time data from Table 1: Data for FCC process.
y1data = np.array([0.5074, 0.3796, 0.2882, 0.1762])    # weight fraction VGO
y2data = np.array([0.3767, 0.4385, 0.4865, 0.5416])    # weight fraction Gasoline
Gasdata = np.array([0.0885, 0.136, 0.1681, 0.2108])    # weight fraction Gas
Cokedata = np.array([0.0274, 0.0459, 0.0572, 0.0714])  # weight fraction Coke
y3data = Gasdata + Cokedata;
ydata = np.array([y1data, y2data, y3data])


# guesses for parameters k1, k2, & k3
k = np.array([0.1, 0.2, 0.3])
params_guess = k;

# initial conditions for ODEs  % initial weight fractions
y0 = np.array([0.51, 0.38, 0.11])

# plot data and the curve fit

plt.axis([0,1,0,1])
plt.plot((1-ydata[:,0],ydata[:,0]),'rx', label='VGO')   #conversion is described as 1-y_1
plt.plot((1-ydata[:,0],ydata[:,1]),'b+', label='Gasoline')   #so the plot will simply convert to
plt.plot((1-ydata[:,0],ydata[:,2]),'g.', label='Gas+Coke')   #conversion on the x-axis.plt.

plt.xlabel('Conversion, wt frac.')
plt.ylabel('Yield, wt frac.')
y_calc = ODE(params,xdata,y0)    #the ODE solver provides the soluti
y1_calc = y_calc[:,0]                  #ons in a 3 by 3 array that must th
y2_calc = y_calc[:,1]                  #en be scanned by the plotter, but
y3_calc = y_calc[:,2]                  #instead the array was unpacked.
plt.plot((1-ydata[:,0]),(y1_calc),'r-', label='Predicted VGO')
plt.plot((1-ydata[:,0]),(y2_calc),'b-', label='Predicted Gasoline')
plt.plot((1-ydata[:,0]),(y3_calc),'g-', label='Predicted Gas+Coke')
plt.show()

axis([0,1,0,1])
plt.plot((xdata,ydata[:,0]),'rx',label='VGO')      #conversion is described as 1-y_1
plt.plot((xdata,ydata[:,1]),'b+',label='Gasoline')      #so the plot will simply convert to
plt.plot((xdata,ydata[:,2]),'g.',label='Gas+Coke')      #conversion on the x-axis.

xlabel('t, hr')
ylabel('Yield, wt frac.')
y_calc = ODE(params,xdata,y0);    #the ODE solver provides the soluti
y1_calc = y_calc[:,0];                  #ons in a 3 by 3 array that must th
y2_calc = y_calc[:,1];                  #en be scanned by the plotter, but
y3_calc = y_calc[:,2];                  #instead the array was unpacked.
plt.plot(xdata,y1_calc,'r-','Predicted VGO' )
plt.plot(xdata,y2_calc,'b-','Predicted Gasoline')
plt.plot(xdata,y3_calc,'g-','Predicted Gas+Coke')
plt.show()

# define the ODEs
# Uses params = [k_1, k_2, k_3]

dy_dt = ODE(t,y,params)
k_1 = params(0)    # assigning names to the three different
k_2 = params(1)    # parameters
k_3 = params(2)

dy1_dt = -1*(k_1+k_3)*y(0)^2  # $\frac{dy_1}{dt}=-(k_2+k_3)*{y_1}^2$
dy2_dt = k_1*y(0)^2-k_2*y(1)  # $\frac{dy_2}{dt}=k_1*{y_1}^2-k_2*y_2$
dy3_dt = k_3*y(0)^2+k_2*y(1)  # $\frac{dy_3}{dt}=k_3*{y_1}^2+k_2*y_2$
#dydt = dydt
#estimate parameters
#[params,resnorm] = curve_fit(@(params,xdata)ODEmodel1(params,xdata,y0),params_guess,xdata,ydata,[], [], OPTIONS);
popt, pcov = curve_fit(ODE,xdata, ydata)

# solving the ODEs
# Uses current params values and xdata as the final point in the tspan for the ODE solver

y_output = ODE(params,xdata,y0)
for i in range(1,len(xdata)):
    t = np.arange(0.,xdata[i],0.001)
    #np.array([~, y_calc]) = odeint(@(t,x)func(t,x,params),t,y0)
    y_calc = odeint(ODE, y0, t, args=(t, x))
    y_output[i,:]=y_calc[end,:]
