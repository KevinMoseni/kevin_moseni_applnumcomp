\documentclass[12pt]{article}

	\usepackage{booktabs}		%for table
	\usepackage{amsmath}			%needed for easy math formulas
	\usepackage{graphicx}		%needed for adding images
	\usepackage{float}			%to place figure as a floating object
	\usepackage{enumitem}		%for manipulating numbered lists
	%\usepackage{tabularx}		%for making pretty table
	%\usepackage[T1]{fontenc}
	%\usepackage[utf8]{inputenc}
	\usepackage{titlesec}		%for adding the dot after the section number
	\titlelabel{\thetitle.\quad}	%for adding the dot after the section number
		
\graphicspath{{images/}}			%to pull figures from images/ folder in .tex directory

\title{\bf{Computational Assignment 1}\\ \bigskip \large{CHE 4990/5110}}
	\date{8-23-2017}
	\author{Kevin Moseni}

\bibliographystyle{abbrv}

\begin{document}
	\maketitle
	\tableofcontents
	
\newpage	

\section{\large{Plasma Science}}
	\label{science}

\paragraph{}
		
      Plasma accounts for over 99 percent of the matter in the universe, but is relatively difficult to produce in Earth's atmosphere. Everyday examples of plasma are lightning, flourescent light bulbs, and fire, but they only occur under certain conditions. Plasmas occur more readily at low pressures, below 1 torr, but with enough energy it is possible to create plasmas at atmospheric pressure. When a gaseous environment is acted on by a plasma, reactions occur between the species of ions and molecules that result from the plasma creating many new species \cite{Chen}. These reactions can be manipulated for engineering applications, and in Section {\ref{apps}}, some applications will be discussed.
\\
\par						
      Based on the kinetic theory of gases, in order to increase the kinetic energy of the particles in a gas to cause an ionizing collision, a high temperature or a high potential can be applied. The Boltzmann Kinetic Theory equation, shown as Equation 1 below, is a useful equation to analyze plasma system.  
\\


\begin{equation}
	\label{eq1}
\frac{\delta{f}}{\delta{t}} =\! \left(\frac{\delta{f}}\delta\right)_{\textrm{force}}\! +\! \left(\frac{\delta{f}}					{\delta{t}}\right)_{\textrm{diffusion}}\! +\! \left(\frac{\delta{f}}{\delta{t}}\right)_{\textrm{collision}}\!
\end{equation}
\\

\par 
	In Equation \ref{eq1}, $f$ is the probability density function in terms of x, y, and z positions and $t$ is time. There is a force exerted externally term, a particle diffusion term, and a collision term. Studying the Boltzmann equation separates the high energy system into parts that would be extremely difficult to study as a whole.
	
\subsection{\small{Fundamentals of Plasma Physics}}

\paragraph{}
		
	The underlying physics of plasma formation is really quite simple. Once the system of gas is energized beyond a certain threshold energy, or given in terms of eV, an ionizing collision occurs. This ionizing collision is between a free electron and one of the gas molecules, which causes a chain reaction of ionizing collisions. One hits another, breaking them into two which both hit two others, which break them into two each... so on and so forth. This threshold voltage is referred to as the {\it{breakdown}} voltage. The Paschen's curve, below, shows the breakdown voltage of different gas species at different pressures. It can be seen here that there is an optimal pressure for the breakdown of gas molecules and the creation of plasma.
	\\
\begin{figure}[t!]
	\caption{Paschen's Curve}
	\centering
	\includegraphics[scale=0.15]{paschen_curves}
\end{figure}	

	In plasma environments, pressure is low enough that it is often synonymous with density. This density represents a cloud of quasi-neutral (net neutral, locally charged) ionized gas particles, in essence a {\it{plasma}}.\
\begin{table}[h!]
	\centering
	\caption{Required Conditions for Atmospheric Pressure Plasma Via Voltage Breakdown with Electrode Gap of 3 mm}
	\bigskip
	\begin{tabular}{c||c||c}
		\toprule
		Pressure (torr) & Temperature (C) & Voltage (V)\\
		\midrule
		760 & 25 & 6000\\
		\bottomrule
	\end{tabular}
\end{table} 
\\	

	Since there are charged particles, there is an electric field that exhibits different characteristics depending on the density of the ionized gas particles. Equation \ref{eq2} below describes the relationship between the electric field and the gas density  
\\

\begin{equation}
	\label{eq2}
\nabla\cdot\bf{E} = \frac{\rho}{\epsilon_o}
\end{equation}	
\\
where $\rho$ represents density, $\epsilon_o$ is the permittivity of free space, and $\bf{E}$ is electric field.
\section{\large{Plasma Applications}}
	\label{apps}
\paragraph{}
	
	As mentioned in Section \ref{science}, plasmas directly alter the chemical composition of any gases that encounter it. This is due to the aforementioned ionizing collisions that occur within the plasma. In low humidity air, a gas composition of 80 to 20 nitrogen to oxygen with trace amounts of water can be assumed, meaning that the neutral gas species present are $\textrm{N}_2$, $\textrm{O}_2$, and $\textrm{H}_2\textrm{O}$. A plasma can cause these three species to react in over 170 different ways to create all sorts of unique molecules and ions at ground and excited states \cite{nature}. For example, in a $\textrm{N}_2$/$\textrm{O}_2$/$\textrm{H}_2\textrm{O}$ environment, a great deal of ozone ($\textrm{O}_3$) is created, suggesting the presence of O singlets. Four of the nearly 200 reactions that occur in a $\textrm{N}_2$/$\textrm{O}_2$/$\textrm{H}_2\textrm{O}$ environment are shown below.


\begin{center}
\begin{minipage}{.5\textwidth}			%Shifts the list to the center of the page before starting the numbering
\begin{enumerate}								%for aesthetics

	\item $\textrm{O}_2 + \textrm{O} \longrightarrow \textrm{O}_3$
	\item ${^-}\textrm{OH} + \textrm{NO}_2	\longrightarrow \textrm{HNO}_2$
	\item $\textrm{O}+ \textrm{H}_2\textrm{O}_2 \longrightarrow \textrm{OH} + \textrm{H}_2\textrm{O}$
	\item $\textrm{N}_2\textrm{O}_4 + \textrm{H}_2\textrm{O} 	\longrightarrow	{\textrm{NO}_2}^-$

\end{enumerate}
\end{minipage}
\end{center}

\subsection{\small{Plasma-Activated Water}}

\paragraph{}

	One example of an application is plasma-activated water, or PAW. PAWs have been shown to be useful for organic waste disposal and bacterial inactivation, but there are questions as to why. PAWs created in an $\textrm{N}_2$/$\textrm{O}_2$/$\textrm{H}_2\textrm{O}$ environment have shown to have a pH as low as 3, suggesting that PAWs are relatively strong acids. Similarly, as mentioned earlier in Section \ref{apps}, the production of ozone has been documented via FTIR. Both strong acids and ozone are destructive towards organics, but a detailed study is needed to determine which effect dominates. 	

\par
	Other than plasma-activated water, plasmas are being used in industry for material development processes such as plasma-aided chemical vapor deposition and sputtering. 

\bibliography{bib_Kevin_Moseni}

\end{document}

