%% Author: Kevin Moseni
% Date: 10/24/2017 CHE 4990: Applied Numerical Computing Computational
% Assignment #4

% Components: VGO, Gasoline, Gas, and Coke FCC: 4 Lump Model
% this program evaluates 4 ODEs to solve for the changing weight fraction of 
% 4 components. This version doesn't combine gas and coke.

%% variables
% t   - time 
% X   - conversion (defined as [1-y_1]) 
% y_1 - VGO 
% y_2 - gasoline
% y_3 - the sum of gas and coke 


function param_estim_4lump
%% data from Table 1: Data for FCC process.
xdata = [1/60, 1/30, 1/20, 1/10]; % time data

%% arranging ydata into a matrix
y1data = [0.5074, 0.3796, 0.2882, 0.1762];   % weight fraction VGO
y2data = [0.3767, 0.4385, 0.4865, 0.5416];   % weight fraction Gasoline
y3data = [0.0885, 0.136, 0.1681, 0.2108];    % weight fraction Gas
y4data = [0.0274, 0.0459, 0.0572, 0.0714];   % weight fraction Coke
ydata = [y1data; y2data; y3data;y4data]';


%% guesses for parameters k_12, k_13, k_14, k_23, & k_24
k(1) = 0.1;
k(2) = 0.2;
k(3) = 0.3;
k(4) = 0.4;
k(5) = 0.4;
params_guess = k;

%% initial conditions for ODEs  % initial weight fractions
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;
y0(4) = 0;

%% options for lsqcurvefit
OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-14, 'TolFun', 1e-14,'StepTolerance',1e-14);

%% estimate parameters
[params,resnorm] = lsqcurvefit(@(params,xdata)ODEmodel1(params,xdata,y0),params_guess,xdata,ydata,[], [], OPTIONS);

%% plot data and the curve fit
figure(1)
hold on
axis([0,1,0,1])
y_calc = ODEmodel1(params,xdata,y0);
plot((1-y_calc(:,1)'),ydata(:,1),'rx')   %conversion is described as 1-y_1
plot((1-y_calc(:,1)'),ydata(:,2),'b+')   %so the plot will simply convert to
plot((1-y_calc(:,1)'),ydata(:,3),'g.')   %conversion on the x-axis.
plot((1-y_calc(:,1)'),ydata(:,4),'m*')
xlabel('Conversion, wt frac.')
ylabel('Yield, wt frac.')
                                        %the ODE solver provides the soluti
y1_calc = y_calc(:,1);                  %ons in a 3 by 3 array that must th
y2_calc = y_calc(:,2);                  %en be scanned by the plotter, but
y3_calc = y_calc(:,3);                  %instead the array was unpacked.
y4_calc = y_calc(:,4);
plot((1-y_calc(:,1))',y1_calc,'r-')
plot((1-y_calc(:,1))',y2_calc,'b-')
plot((1-y_calc(:,1))',y3_calc,'g-')
plot((1-y_calc(:,1))',y4_calc,'m-')

hold off
legend('VGO','Gasoline','Gas','Coke','Predicted VGO','Predicted Gasoline','Predicted Gas','Predicted Coke')
export_fig(sprintf('CA4_four_lump_conversion'), '-png');

figure(2)
hold on
axis([0,1,0,1])
plot(xdata',ydata(:,1),'rx')      %conversion is described as 1-y_1
plot(xdata',ydata(:,2),'b+')      %so the plot will simply convert to
plot(xdata',ydata(:,3),'g.')      %conversion on the x-axis.
plot(xdata',ydata(:,4),'m*')
xlabel('t, hr')
ylabel('Yield, wt frac.')
y_calc = ODEmodel1(params,xdata,y0);    %the ODE solver provides the soluti
y1_calc = y_calc(:,1);                  %ons in a 3 by 3 array that must th
y2_calc = y_calc(:,2);                  %en be scanned by the plotter, but
y3_calc = y_calc(:,3);                  %instead the array was unpacked.
y4_calc = y_calc(:,4);
plot(xdata',y1_calc,'r-')
plot(xdata',y2_calc,'b-')
plot(xdata',y3_calc,'g-')
plot(xdata',y4_calc,'m-')
hold off
legend('VGO','Gasoline','Gas','Coke','Predicted VGO','Predicted Gasoline','Predicted Gas','Predicted Coke')
export_fig(sprintf('CA4_four_lump_time'), '-png');

end

%% define the ODEs
% Uses params = [k_12, k_13, k_14, k_23, & k_24]

function dydt = ODE(t,y,params)
k_12 = 39.1;    % assigning names to the three different
k_13 = 10.1;    % parameters
k_14 = 3.10;
k_23 = 1.29;
k_24 = 0.671;

dydt(1) = -1*(k_12+k_13+k_14)*y(1)^2;       % $\frac{dy_1}{dt}=-(k_12+k_13+k_14)*{y_1}^2$
dydt(2) = k_12*y(1)^2-k_23*y(2)-k_24*y(2);  % $\frac{dy_2}{dt}=k_12*{y_1}^2-k_23*y_2-k_24*y_2$
dydt(3) = k_13*y(1)^2+k_23*y(2);            % $\frac{dy_3}{dt}=k_13*{y_1}^2+k_23*y_2$
dydt(4) = k_14*y(1)^2+k_24*y(2);            % $\frac{dy_4}{dt}=k_14*{y_1}^2+k_24*y_2$
dydt = dydt';   % reorient the matrix
end

%% solving the ODEs
% Uses current params values and xdata as the final point in the tspan for
% the ODE solver

function y_output = ODEmodel1(params,xdata,y0)
for i = 1:length(xdata)
    tspan = [0:0.001:xdata(i)];
    [~,y_calc] = ode23s(@(t,x) ODE(t,x,params),tspan,y0);
    y_output(i,:)=y_calc(end,:);
end
end