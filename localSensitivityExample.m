function localSensitivityExample
a = 1;
b = 1;
c = 1;
d = 1;
parameters = [a b c d];
%x = 0:0.1:2;
x = 2; % point to evaluate our output for sensitivity 
percentParamChange = 20;
fAtParameters = model(x,parameters); % (f at parameters)


for i = 1:length(parameters) % this for-loop is doing one-at-a-time.
    newparameters = parameters;
    newparameters(i) = parameters(i)*(1+percentParamChange/100);
    output(i) = model(x,newparameters);
    sensitivity(i) = (output(i) - fAtParameters)/fAtParameters/percentParamChange;
end    
%plot(x, output)

output % display the raw output vector
sensitivity % display the sensitivity vector

    


function f = model(x,parameters)
a = parameters(1);
b = parameters(2);
c = parameters(3);
d = parameters(4);

f = a*exp(b*x) + c*exp(d*x);

