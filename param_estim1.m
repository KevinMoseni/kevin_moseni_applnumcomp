%Author: Kevin Moseni
%Date: 10/9/2017

%%Lecture 12: Parameter Estimation Example 1
function param_estim1
tdata = [0 0.5 1 5 30]';
xdata = [0 0.5 1.2 2.5 2.7];

x0 = 0;
b1guess = 1;
b2guess = 2;
parameterguesses = [b1guess, b2guess];

    function output = ODEmodel(parameters, t)
        
        [tsoln,xsoln] = ode45(@(t,x)deriv(t,x,parameters),t, x0);    
        output = xsoln';    %the array needed to be reoriented
    end

    function dxdt = deriv(t,x,parameters)           
        b1 = parameters(1);
        b2 = parameters(2);
        dxdt = b1-b2*x;
    end

[parametersoln, resnorm,residual,exitflag,output] = lsqcurvefit(@ODEmodel, parameterguesses, tdata, xdata)
end

