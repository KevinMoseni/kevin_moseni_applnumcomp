# -*- coding: utf-8 -*-
"""
This .py file is to mimic the functionality of system_of_ODEs.m

this is an ICP 

Created on Wed Sep 13 08:16:05 2017

@author: kevin

%% Function: output
%% Author: Kevin Moseni
===============================================================================
Notes:
    
    for modules (in order to call a function in the console when the .py file 
    isn't open), look them up as they are useful for high performance computing
    
    python assertions:              (more python tips on the .m file)
        assert statement, "msg"
"""
import numpy as np

def system_of_ODEs(t=0,C=[6.25,0],k1=0.15,k2=0.6,k3=0.1,k4=0.2):

"""to run this option, add one quoutation mark after this statement""
    
    assert t>=0, "time can only be positive"
    C_A = C[0]
    C_B = C[1]
    
    
    dCA_dt = -k1*C_A-k2*C_B    
    dCB_dt = k1*C_A-k3-k4*C_B

    return [dCA_dt,  dCB_dt]
"""
def system_of_ODEs(t=0,C=[6.25,0],params = [0.15,0.6,k3=0.1,k4=0.2]):
    
    assert t>=0, "time can only be positive"
    C_A, C_B, = C
    k1,k2,k3,k3 = params
    
    dCA_dt = -k1*C_A-k2*C_B    
    dCB_dt = k1*C_A-k3-k4*C_B

    return np.array([ (dCA_dt,  dCB_dt) ])

