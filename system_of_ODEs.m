%% Description
%This function can evaluate the derivatives in a system of ODEs describing
%mass concentrations of arbitrary species A and B subject.

%the code determines if either: a)all variables and parameters were
%provided b)t & C were provided by the user and defualt k-values are needed
%c)k-values were provided by the user and default t & C values are needed

%the code then evaluates the following derivatives

%$$\frac{dC_A}{dt} =\! {-k_1}{C_A} -\! \ {k_2}{C_B}$$ $$\frac{dC_B}{dt} =\!
%{k_1}{C_A} -\! \ k_3 -\! \ {k_4}{C_B}$$

%Ca is concen. of species A; Cb is concen. of species B; k1, k2, k3, k4 are
%rate parameters t is a single time value; C is a 1 x 2 matrix (row vector)
%[concen. of species A, concen. of species B]; k1, k2, k3, and k4 are
%parameter values; output is a 2 x 1 matrix (column vector) of differential
%equations evaluated at time t

%% Function: output
%% Author: Kevin Moseni

function output = system_of_ODEs(varargin)      %must be same as file name

%% Finding parameter and variable values from user input or default values

if nargin < 3           %if only t and C  given, use default values of k
    k1 = 0.15;              %in 1/h
    k2 = 0.6;               %in 1/h
    k3 = 0.1;               %in mg/L h
    k4 = 0.2;               %in 1/h
elseif nargin == 6      %if all 6 are specified, they are matched
    t  = varargin{1};   %based on t,C,k1,k2,k3,k4
    C  = varargin{2};   %all are specified in the function call, all elements are connected
    k1 = varargin{3};   %to the proper variable and parameter names in
    k2 = varargin{4};   %vargarin()
    k3 = varargin{5};
    k4 = varargin{6};
else
    disp('Error: Must have 6 arguments')
    beep
end
if nargin == 0           %if nothing is specified
    t = 0;                   %in h
    C = [6.25 0];            %in mg/L
else
    t = varargin{1};
    C = varargin{2};
end

%% Algebraic calculation of CA/dt and CB/dt

% With the equations for $$\frac{dC_A}{dt}$$ and $$\frac{dC_B}{dt}$$
%// $$\frac{dC_A}{dt} =\! {-k_1}{C_A} -\! \ {k_2}{C_B}$$ becomes $$C_A =\!
%{-k_1}{C_AO]{t} -\! \ {k_2}{C_BO}{t} +\! \ {C_AO}$$ $$\frac{dC_B}{dt} =\!
%{k_1}{C_A} -\! \ k_3 -\! \ {k_4}{C_B}$$ becomes $$C_B =\! {k_1}{C_AO]{t}
%-\! \ {k_3}{t} -\! \ {k_4}{C_BO}{t} +\! {C_AO}$$ //
% Ca = -k1*C(1,1)*t-k2*C(1,2)*t + C(1,1); Cb =
% k1*C(1,2)*t-k3*t-k4*C(1,2)*t+C(1,2);

dCAdt = -k1*C(1,1)-k2*C(1,2);       %dCAdt and dCBdt evaluated using either input or default values  default
dCBdt = k1*C(1,2)-k3-k4*C(1,2);

%% Orienting solutions into a 2 x 1 matrix

output = [dCAdt dCBdt];             %output is printed

end

