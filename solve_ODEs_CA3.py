# -*- coding: utf-8 -*-
"""
this function defines a system of oridinary differential equations for
dFA/dV, dFB/dV, dFC/dV, and dT/dV. This describes a non-isothermal PFR.
The reactions are at S.S. but are not spatially uniform, thus volume (V)
is the indepepent variable.

The chemical reactions are modeled as:
         
         A --k1--> B       -r1A  = k1A*CA 
         2A --k2--> C      -r2A  = k2A*CA^2

Author: Kevin Moseni Computational Assignment 3
"""

import numpy as np #needed for all numerical methods
from scipy.integrate import odeint #this is essentially ode45
import matplotlib.pyplot as plt #for plotting

def solve_ODEs_CA3():            
    def ODEs_CA3(V, F_and_T):   #Unpack user input
        FA = F_and_T[0] #user input Flow rate for species A [mol s^-1] *******THIS IS WHERE THE ERROR POINTS*******
        FB = F_and_T[1] #user input Flow rate for species B [mol s^-1]
        FC = F_and_T[2] #user input Flow rate for species C [mol s^-1]
        T = F_and_T[3]  #user input for V [mol s^-1]
        
        #Parameter values       #these values were provided, did not manage to 
        E1_over_R = 4000 #[K]    incorporate the variable inputs by the user.
        E2_over_R = 9000 #[K]
        Ua = 4000   #[J m^-3 s^-1 C^-1]
        Ta = 100    #[C] (Constant)            
        deltaHRx1A = -20000 #[J (moles of A reacted in rxn 1)^-1]
        deltaHRx2A = -60000 #[J (moles of A reacted in rxn 2)^-1]            
        CPA = 90    #[J mol^-1 C^-1]
        CPB = 90    #[J mol^-1 C^-1]    Specific heats of components 
        CPC = 180   #[J mol^-1 C^-1]
        
        #Problem statement givens
        CT0 = 0.1   #[J mol^-1 C^-1]    Total molar flow rate
        T0 = 423    #[K] = 150+273      Feed temperature
        
        #k1A and k2A
        k1A = 10*exp(E1_over_R*((1/300) - (1/T)))     #[s^-1]
        k2A = 0.09*exp(E2_over_R*((1/300) - (1/T)))   #[dm^3 mol^-1 s^-1]
        
        #Stoichiometry (gas phase deltaP = 0)
        FT = FA + FB + FC       #(E12-5.13)
        CA = CT0*(FA/FT)*(T0/T) #(E12-5.10)
        CB = CT0*(FB/FT)*(T0/T) #(E12-5.11)
        CC = CT0*(FC/FT)*(T0/T) #(E12-5.12)
        
        #Reaction rates for individual reactions
        r1A = -k1A*CA      #(E12-5.1) 
        r2A = -k2A*CA^2    #(E12-5.2)    
        r1B = k1A*CA       #(E12-5.8)
        r2C = 1/2*k2A*CA^2 #(E12-5.9)
        
        #Overall reaction rates for components
        rA = r1A + r2A     #(E12-5.7)
        rB = r1B           #(E12-5.8)
        rC = r2C           #(E12-5.9)
        
        #Differential Equations
        # Flowrates w.r.t. volume
        dFA_dV = rA   #(E12-5.4) note: dFA_dC = rA = -k1A*CA-k2A*CA^2
        dFB_dV = rB   #(E12-5.5) note: dFB_dV = rB = k1A*CA
        dFC_dV = rC   #(E12-5.6) note: dFC_dV = rC = 1/2*k2A*CA^2
        
        #Non-isothermal conditions
        dT_dV = (Ua*(Ta-T)+(-r1A)*(-deltaHRx1A)+(-r2A)*(-deltaHRx2A))/(FA*CPA+FB*CPB+FC*CPC) #(E12-5.3)
        
        #Output vector
        F_and_Tdot = [dFA_dV, dFB_dV, dFC_dV, dT_dV]   #This is the solution to the ODE in a 4 x 1 matrix                     
        
        return F_and_Tdot

    #Range of V and step 
    Vrange = np.linspace(0, 1, 101)
    
    #Defining inital values
    FA0 = 100
    FB0 = 0
    FC0 = 0
    T0  = 423
    F_and_Tinitial = [FA0, FB0, FC0, T0]
    
    #Calling odeint to solve ODEs
    F = odeint(ODEs_CA3, F_and_Tinitial, Vrange) #args = (params,)) #v range and F_and_T initial values provided within
    
    #Output vectors
    FAsoln = F[:,0] #unpacking FA, FB, FC, and T from the solution matrix 
    FBsoln = F[:,1] #that was produced by odeit and put into F.
    FCsoln = F[:,2]
    Tsoln  = F[:,3]
    
    #Plotting
    line1 = plt.plot(Vrange,FAsoln, '-', label='$FA$')  #these were done following
    line2 = plt.plot(Vrange,FBsoln, '--', label='$FB$') #the lecture 9 example
    line3 = plt.plot(Vrange,FCsoln, 'o-', label='$FC$')
    plt.legend()
    plt.xlabel('V [L]')
    plt.ylabel('Fi [K]')
    
    plt.show()
    
    line1 = plt.plot(Vrange, Tsoln, '-',label='$T$')
    plt.legend()
    plt.xlabel('V [L]')
    plt.ylabel('T [K]')
    plt.show()
    
solve_ODEs_CA3()

 