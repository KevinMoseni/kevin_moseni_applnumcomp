function varargout = DefineExplicitEquation(varargin)
% DEFINEEXPLICITEQUATION MATLAB code for DefineExplicitEquation.fig
%      DEFINEEXPLICITEQUATION, by itself, creates a new DEFINEEXPLICITEQUATION or raises the existing
%      singleton*.
%
%      H = DEFINEEXPLICITEQUATION returns the handle to a new DEFINEEXPLICITEQUATION or the handle to
%      the existing singleton*.
%
%      DEFINEEXPLICITEQUATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEEXPLICITEQUATION.M with the given input arguments.
%
%      DEFINEEXPLICITEQUATION('Property','Value',...) creates a new DEFINEEXPLICITEQUATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DefineExplicitEquation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DefineExplicitEquation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DefineExplicitEquation

% Last Modified by GUIDE v2.5 07-Dec-2017 14:39:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DefineExplicitEquation_OpeningFcn, ...
                   'gui_OutputFcn',  @DefineExplicitEquation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DefineExplicitEquation is made visible.
function DefineExplicitEquation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DefineExplicitEquation (see VARARGIN)

% Choose default command line output for DefineExplicitEquation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DefineExplicitEquation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DefineExplicitEquation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function ExpLHS_Callback(hObject, eventdata, handles)
% hObject    handle to ExpLHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExpLHS as text
%        str2double(get(hObject,'String')) returns contents of ExpLHS as a double

% extract user input
handles.ExpLHS = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ExpLHS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExpLHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ExpRHS_Callback(hObject, eventdata, handles)
% hObject    handle to ExpRHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExpRHS as text
%        str2double(get(hObject,'String')) returns contents of ExpRHS as a double

% extract user input
handles.ExpRHS = get(hObject,'String');
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ExpRHS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExpRHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonSubmitExplicitEQ.
function ButtonSubmitExplicitEQ_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSubmitExplicitEQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% .mat for transfer
load('UserInputsDE.mat')
ExpLHS = handles.ExpLHS;
ExpRHS = handles.ExpRHS;
save('UserInputsDE.mat','ExpLHS','ExpRHS')
close

